<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--favicon icon-->
    <link rel="icon" href="<?php echo base_url();?>assets/assets/img/favicon.png" type="image/png" sizes="16x16">

    <!--title-->
    <title>Ode Pulsa</title>

    <!--build:css-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assets/css/main.css">
    <!-- endbuild -->
</head>

<body>


    <!--preloader start-->
    <div id="preloader">
        <div class="preloader-wrap">
            <img src="<?php echo base_url();?>assets/assets/img/logo-color-new.png" alt="logo" style="width:250px" class="img-fluid" />
            <div class="thecube">
                <div class="cube c1"></div>
                <div class="cube c2"></div>
                <div class="cube c4"></div>
                <div class="cube c3"></div>
            </div>
        </div>
    </div>
    <!--preloader end-->
    <!--header section start-->
    
    <header class="header">
        <!--start navbar-->

        <nav class="navbar navbar-expand-lg fixed-top bg-transparent">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <img src="<?php echo base_url();?>assets/assets/img/logo-white-new.png" alt="logo" class="img-fluid logo-media-phone" />
                </a>
                <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="ti-menu"></span>
                </button> -->

                <!-- <div class="collapse navbar-collapse h-auto" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto menu">
                        <li><a href="index-5.html#" class="dropdown-toggle"> Home</a>
                            <ul class="sub-menu">
                                <li><a href="index.html">Home Page 01</a></li>
                                <li><a href="index-2.html">Home Page 02</a></li>
                                <li><a href="index-3.html">Home Page 03</a></li>
                                <li><a href="index-4.html">Home Page 04</a></li>
                                <li><a href="index-5.html">Home Page 05</a></li>
                                <li><a href="index-6.html">Home Page 06</a></li>
                                <li><a href="index-7.html">Home Page 07</a></li>
                                <li><a href="index-8.html">Home Page 08</a></li>
                                <li><a href="index-9.html">Home Page 09</a></li>
                            </ul>
                        </li>
                        <li><a href="index-5.html#about" class="page-scroll">About</a></li>
                        <li><a href="index-5.html#features" class="page-scroll">Features</a></li>
                        <li><a href="index-5.html#" class="dropdown-toggle">Pages</a>
                            <ul class="sub-menu">
                                <li><a href="index-5.html#" class="dropdown-toggle-inner">Login & Sign Up</a>
                                    <ul class="sub-menu">
                                        <li><a href="login.html">Login Page</a></li>
                                        <li><a href="sign-up.html">Signup Page</a></li>
                                        <li><a href="password-reset.html">Reset Password</a></li>
                                    </ul>
                                </li>
                                <li><a href="index-5.html#" class="dropdown-toggle-inner">Utilities</a>
                                    <ul class="sub-menu">
                                        <li><a href="faq.html">FAQ Page</a></li>
                                        <li><a href="404.html">404 Page</a></li>
                                        <li><a href="coming-soon.html">Coming Soon</a></li>
                                        <li><a href="thank-you.html">Thank You Page</a></li>
                                        <li><a href="download.html">Download Page <span class="badge accent-bg text-white">New</span></a></li>
                                        <li><a href="review.html">Review Page <span class="badge accent-bg text-white">New</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="index-5.html#" class="dropdown-toggle-inner">Team</a>
                                    <ul class="sub-menu">
                                        <li><a href="team.html">Our Team Members</a></li>
                                        <li><a href="team-single.html">Team Member Profile</a></li>
                                    </ul>
                                </li>
                                <li><a href="index-5.html#" class="dropdown-toggle-inner">Our Blog</a>
                                    <ul class="sub-menu">
                                        <li><a href="blog-default.html">Blog Grid</a></li>
                                        <li><a href="blog-no-sidebar.html">Blog No Sidebar</a></li>
                                        <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                        <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                        <li><a href="blog-single-left-sidebar.html">Details Left Sidebar</a></li>
                                        <li><a href="blog-single-right-sidebar.html">Details Right Sidebar</a></li>
                                    </ul>
                                </li>
                                <li><a href="about-us.html">About Us </a></li>
                                <li><a href="contact-us.html">Contact Us</a></li>
                            </ul>
                        </li>
                        <li><a href="index-5.html#screenshots" class="page-scroll">Screenshots</a></li>
                        <li><a href="index-5.html#process" class="page-scroll">Process</a></li>
                        <li><a href="index-5.html#pricing" class="page-scroll">Pricing</a></li>
                        <li><a href="index-5.html#contact" class="page-scroll">Contact</a></li>
                    </ul>
                </div> -->
            </div>
        </nav>
    </header>
    <!--header section end-->

    <div class="main">
        <!--hero section start-->
        <!--hero section start-->
        <section class="position-relative bg-image pt-100" image-overlay="10">
            <div class="background-image-wraper"></div>
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-10 col-lg-6">
                        <div class="section-heading text-white py-5">
                            <h1 class="text-white font-weight-bold">Selamat datang di<br> Ode Pulsa</h1>
                            <h3 class="text-white font-weight-bold">Server Pulsa, Data Internet, PLN Token, Game dan PPOB Terbesar, Terlengkap, Terpercaya di Indonesia.</h3>
                            <div class="action-btns mt-4">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="#" class="d-flex align-items-center app-download-btn btn btn-white btn-rounded">
                                            <span class="fab fa-apple icon-size-sm mr-3 color-primary"></span>
                                            <div class="download-text text-left">
                                                <small>Coming Soon</small>
                                                <h5 class="mb-0">App Store</h5>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="<?php echo base_url()?>home/download/android" class="d-flex align-items-center app-download-btn btn btn-white btn-rounded">
                                            <span class="fab fa-google-play icon-size-sm mr-3 color-primary"></span>
                                            <div class="download-text text-left">
                                                <small>Download form</small>
                                                <h5 class="mb-0">Google Play</h5>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-5">
                        <div class="img-wrap">
                            <img src="<?php echo base_url();?>assets/assets/img/app-mobile-image.png" alt="shape" class="img-fluid">
                        </div>
                    </div>
                </div>
                <!--end row-->
                <div class="row mb-5">
                    <ul class="list-inline counter-wrap bg-white" >
                        <li class="list-inline-item">
                            <div class="single-counter text-center count-data">
                                <span class="color-primary count-number">50,232</span>
                                <h6>Jumlah Member</h6>
                            </div>
                        </li>
                        <li class="list-inline-item">
                            <div class="single-counter text-center count-data">
                                <span class="color-primary count-number">30,170</span>
                                <h6>App Download</h6>
                            </div>
                        </li>
                        <li class="list-inline-item">
                            <div class="single-counter text-center count-data">
                                <span class="color-primary count-number">30</span>
                                <h6>Kerjasama Vendor</h6>
                            </div>
                        </li>
                        <li class="list-inline-item">
                            <div class="single-counter text-center count-data">
                                <span class="color-primary">5/5</span>
                                <h6>Rating</h6>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!--hero section end-->
        <!--hero section end-->
