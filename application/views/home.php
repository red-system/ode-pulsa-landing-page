

        <a href="https://api.whatsapp.com/send?phone=6281246104772" target="blank" class="act-btn">
            <img style="width:25px; margin-bottom:15px;"src="<?php echo base_url();?>assets/assets/img/whatsapp.png"/>
        </a>
        
        <!--promo section start-->
        <section class="promo-section ptb-100 position-relative overflow-hidden">
            <div class="container" style="margin-top:-30px">
            
            <br>
            <br>
                <div class="row">
                    <div class="col-lg-7 col-md-10">
                        <div class="section-heading">
                            <h2>Keuntungan Menjadi Member</h2>
                            <p>Odepulsa memberikan keuntungan yang sangat menarik bagi semua member</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="card border-0 single-promo-card single-promo-hover-2 p-2 mt-4">
                            <div class="card-body">
                                <div class="pb-2">
                                    <span class="fas fa-donate icon-size-md color-secondary"></span>
                                </div>
                                <div class="pt-2 pb-3">
                                    <h5>Harga Master Murah</h5>
                                    <p class="mb-0">Odepulsa memberikan harga murah dan bersaing</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card border-0 single-promo-card single-promo-hover-2 p-2 mt-4">
                            <div class="card-body">
                                <div class="pb-2">
                                    <span class="fas fa-gift icon-size-md color-secondary"></span>
                                </div>
                                <div class="pt-2 pb-3">
                                    <h5>Poin Berhadiah</h5>
                                    <p class="mb-0">Reward/Penghargaan berupa POIN BERHADIAH</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card border-0 single-promo-card single-promo-hover-2 p-2 mt-4">
                            <div class="card-body">
                                <div class="pb-2">
                                    <span class="fas fa-crown icon-size-md color-secondary"></span>
                                </div>
                                <div class="pt-2 pb-3">
                                    <h5>Bonus Get Member</h5>
                                    <p class="mb-0">Daftarkan Downline dan dapatkan hadiah saldonya</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card border-0 single-promo-card single-promo-hover-2 p-2 mt-4">
                            <div class="card-body">
                                <div class="pb-2">
                                    <span class="fas fa-mobile-alt icon-size-md color-secondary"></span>
                                </div>
                                <div class="pt-2 pb-3">
                                    <h5>Aplikasi Mobile</h5>
                                    <p class="mb-0">Support system yang akan membantu siapa saja</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--promo section end-->

        <!--about us section start-->
        <section id="about" class="about-us ptb-100 position-relative gray-light-bg">
            <div class="container">
                <div class="row align-items-center justify-content-lg-between justify-content-md-center">
                    <div class="col-md-5 col-lg-4">
                        <div class="about-content-right">
                            <img src="<?php echo base_url();?>assets/assets/img/app-mobile-image-2.png" alt="about us" class="img-fluid gambar-resize">
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-7">
                        <div class="about-content-left section-heading">
                            <h2>Kelebihan menggunakan aplikasi</h2>

                            <ul class="check-list-wrap pt-3">
                                <li><strong>Aman</strong> – Tingkat keamanan aplikasi tinggi karena menggunakan teknologi enkripsi data sehingga membuat kerahasiaan data aman dan terjamin.</li>
                                <li><strong>Fleksibel</strong> – Tidak ada minimal saldo sehingga siapapun dapat mulai bergabung bersama Odepulsa.</li>
                                <li><strong>Transaksi</strong> – Mengakomodir segala jenis transaksi mulai dari Pulsa, Paket Data, Voucher Prabayar, BPJS, PLN dan sebagainya.</li>
                                <li><strong>Fitur Lengkap</strong> – Segala jenis transaksi dapat dilakukan melalui aplikasi seperti : Pembelian Pulsa, Paket Data, Voucher Game, BPJS, Token PLN, dan Sebagainya.</li>
                            </ul>
                            <div class="action-btns mt-4">
                                    <li class="list-inline-item">
                                        <a href="<?php echo base_url()?>home/download/android" class="d-flex align-items-center app-download-btn btn btn-white btn-rounded">
                                            <span class="fab fa-google-play icon-size-sm mr-3 color-primary"></span>
                                            <div class="download-text text-left">
                                                <small>Download form</small>
                                                <h5 class="mb-0">Google Play</h5>
                                            </div>
                                        </a>
                                    </li>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--about us section end-->

        <!--our contact section start-->
        <section id="contact" class="contact-us-section ptb-100">
            <div class="container">
                <div class="row justify-content-around">
                    <div class="col-12 pb-3 message-box d-none">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="col-md-12 col-lg-5 mb-5 mb-md-5 mb-sm-5 mb-lg-0">
                        <div class="contact-us-form gray-light-bg rounded p-5">
                            <h4>Pertanyaan seputar Odepulsa</h4>
                            <form action="" method="POST" id="contactForm" class="contact-us-form">
                                <div class="form-row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Masukan Nama" required="required">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="Masukan Email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="form-control" rows="7" cols="25" placeholder="Pesan anda . . ."></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mt-3">
                                        <button type="submit" class="btn btn-brand-02" id="btnContactUs">
                                            Kirim ke Odepulsa
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="contact-us-content">
                            <h2>Tertarik?</h2>
                            <p class="lead">Pelajari bagaimana berbisnis dengan Odepulsa dan dapatkan segala keuntunganya</p>

                            <a href="https://api.whatsapp.com/send?phone=6281246104772" target="blank" class="btn btn-outline-brand-01 align-items-center">Pelajari Selengkapnya <span class="ti-arrow-right pl-2"></span></a>

                            <hr class="my-5">

                            <ul class="contact-info-list">
                                <li class="d-flex pb-3">
                                    <div class="contact-icon mr-3">
                                        <span class="fas fa-location-arrow color-primary rounded-circle p-3"></span>
                                    </div>
                                    <div class="contact-text">
                                        <h5 class="mb-1">Lokasi Perusahaan</h5>
                                        <a href="https://goo.gl/maps/kBGabthSo3piKDmC8" target="blank">
                                            Jl. Gatot Subroto 1/VI No. 12, Denpasar – Bali
                                        </a>
                                    </div>
                                </li>
                                <li class="d-flex pb-3">
                                    <div class="contact-icon mr-3">
                                        <span class="fas fa-envelope color-primary rounded-circle p-3"></span>
                                    </div>
                                    <div class="contact-text">
                                        <h5 class="mb-1">Email Perusahaan</h5>
                                        <a href="mailto:info@odepulsa.co" target="blank">
                                            info@odepulsa.co
                                        </a>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--our contact section end-->
 